# Scala school #

### Setup ###

- In IntelliJ install plugins SBT and probably Scala
- Clone this repo:

**git clone** https://lstolowski@bitbucket.org/lstolowski/scala-school.git

- **Do not push anything** to this repo - solve the exercises at your own


### Requirements for exercises ###

* Use Scala only
* Try to be **pure functional**: don't use var variables, use immutable objects, be stateless
* Try to solve exercises without StackOverflow help ;)
* Write tests


### Meetings ###

* **Class 1**: Collections and recursion
* ...