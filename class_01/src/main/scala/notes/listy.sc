import scala.annotation.tailrec

val ls = List[Int](1, 2, 3, 4)
val ls2 = 1 :: 2 :: Nil

ls.sortWith((a, b) => a < b)

ls.foldRight("")((b, a) => b + a)

abstract class Letter
case class A(x: Int) extends Letter
case class B(x: Int) extends Letter

def matchMet(x: List[Int]): String = x match {
  case head :: second :: rest => "two = " + second
  case head :: rest => "=" + head
  case Nil  => "="
}
matchMet(List(2, 3, 4, 5))
matchMet(List(2))
matchMet(Nil)
@tailrec
def findEvens(ls: List[Int], acc: List[Int]): List[Int] = ls match{
  case head :: rest =>
    if(head % 2 == 0)
      findEvens(rest, head :: acc)
    else
      findEvens(rest, acc)

  case Nil => acc
}
findEvens(List(1,2,3,4,5,6,7), Nil)


