package exercises


/**
  * Collections in Scala
  *
  * Set 1.
  *
  * Simple warm up with collections and recursion
  *
  */
object CollectionsInScala {


  /**
    * using pattern matching, find last element on list
    * @param ls
    * @return last element of list or throw an NoSuchElementException when ls.isEmpty
    */
  def findLast(ls: List[Any]): Any = ???

  /**
    * Find N-th element from the end where n==1 is the last element, n==2 is one before last and so on.
    *
    * ! Ensure your solution is tail recursive (use @tailrec annotation)
    *
    * @param ls
    * @return nth before end
    */
  def findNthBeforeEnd(ls: List[Any], n: Int): Any = ???


  /**
    * Is the given list a palidrome? (example palidrome: 1 2 3 4 3 2 1)
    * Write tests in CollectionsInScala$Test
    * @param ls
    * @return
    */
  def isAPalidrome(ls: List[Any]): Boolean = ???

  /**
    * Change the list (return new instance) to be unique. Keep the initial order
    *
    * Try to be tail recursive!
    *
    * @param ls
    * @return list of unique elems
    */
  def uniqueList(ls: List[Any]): List[Any] = ???


}
