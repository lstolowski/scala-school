package exercises


/**
  * Collections in Scala
  *
  * Set 2.
  *
  * HDR photography settings
  */
object HDRPhotography {

  /**
    * Photo with a given light value
    */
  type PhotoLight = Double

  /**
    * Photo with original light value (PhotoLight) + values with added and subtracted EV values
    */
  type HDR = List[PhotoLight]

  /**
    * Simplified picture
    */
  type Picture = Char

  /**
    * A photo representation
    *
    * @param light
    * @param pic
    */
  case class Photo(light: PhotoLight, pic: Picture)


  /**
    * For given list of light settings, create List of settings for HDR photos.
    * @param light
    * @param ev
    * @param gradient
    * @return
    *         For original light 8 and ev = 0.5, gradient = 5 should return list of [5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5]
    *         with kept order (original light value should be in the middle)
    */
  def hdrPhoto(light: PhotoLight, ev: Double, gradient: Int): HDR = ???


  /**
    * Transforms a Picture to a HDR version using the following alghoritm:
    *
    * newLight = product(HDR)
    * newPic = ( (pic as Int) - ( newLight % (pic as Int) ) ) as Char
    * where function product = a * b * c ... * n
    *
    * hdrTransformation(Picture(oldLight, pic), HDR) --> Picture(newLight, newPic)
    *
    * @param pic
    * @param hdr
    * @return
    */
  def hdrTransformation(pic: Picture, hdr: HDR): Picture = ???


  /**
    * Process a list of photos with HDR
    *
    * @param photos
    * @param ev
    * @param gradient
    * @return
    */
  def processPhotos(photos: List[Photo], ev: Double, gradient: Int): List[Photo] = ???

}
