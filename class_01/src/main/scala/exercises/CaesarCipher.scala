package exercises

/**
  * Collections in Scala
  *
  * Set 3
  *
  * Implement Caesar cipher
  */
object CaesarCipher {

  /**
    * lowercase characters [a-z] take part in encoding and decoding
    */
  val characters = 'a' to 'z'

  /** Directions of encoding */
  sealed trait Direction
  case object Right extends Direction   /* for key=1 b encodes to c, z encodes to a, default */
  case object Left extends Direction    /* for key=1 b encodes to a, a encodes to z  */

  /**
    * Encodes text using Caesar cipher, default direction is Right.
    * @param txt text to be ciphered
    * @param key number of letters to be shifted
    * @return encoded
    */
  def encode(txt: String, key: Int): String = ???

  /**
    * Encodes text using Caesar cipher with given direction
    * @param txt text to be ciphered
    * @param key number of letters to be shifted
    * @param dir shifting direction
    * @return encoded
    */
  def encode(txt: String, key: Int, dir: Direction): String = ???

  /**
    * Decode encoded txt to the original word. Assume here encoding in Right direction
    * @param encoded
    * @param key
    * @return
    */
  def decode(encoded: String, key: Int): String = ???

  /**
    * Decode encoded txt to the original word.
    * @param encoded
    * @param key
    * @param dir direction of encoding
    * @return
    */
  def decode(encoded: String, key: Int, dir: Direction): String = ???


}
