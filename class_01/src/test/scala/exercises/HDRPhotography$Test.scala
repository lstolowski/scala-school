package exercises

import org.scalatest.FunSuite

import HDRPhotography._
/**
  * @author lstolowski 
  * @since 15.11.16
  */
class HDRPhotography$Test extends FunSuite {

  test("hdr for light = 8 , ev = 0.5 and gradient 4") {

    val given: PhotoLight = 8.0
    val expected: HDR = List(6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10)

    assert(hdrPhoto(given, 0.5, 4) === expected)
  }

  test("pictures to hdr") {
    val ugly = List(Photo(5, 'a'), Photo(7, 's'))
    val pretty = List(Photo(6, '%'), Photo(50, 'A'))

    assert(processPhotos(ugly, 0.5, 2).toSet == pretty.toSet )


  }

  // TODO write more tests for edge cases
}
