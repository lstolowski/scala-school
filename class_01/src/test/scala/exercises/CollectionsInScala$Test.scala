package exercises

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import CollectionsInScala._

@RunWith(classOf[JUnitRunner])
class CollectionsInScala$Test extends FunSuite {

  test("findLast: last on list") {
    assert(findLast(List(1, 2, 3, 4, 5, 10)) === 10)
  }

  test("findLast: on empty list should throw an exception") {
    intercept[NoSuchElementException] {
      findLast(List())
    }
  }

  test("findNthBeforeEnd: last on list") {
    assert(findNthBeforeEnd(List(1, 2, 3, 4, 5, 10), 1) === 10)
  }

  test("findNthBeforeEnd: last 3th on list") {
    assert(findNthBeforeEnd(List("one", "two", "three", "four", "five", "six"), 3) === "four")
  }

  test("findNthBeforeEnd: non existing elem on list should throw exception") {
    intercept[NoSuchElementException] {
      findNthBeforeEnd(List(1, 2), 3)
    }
  }



}
