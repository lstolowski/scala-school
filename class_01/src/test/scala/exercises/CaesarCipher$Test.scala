package exercises

import org.scalatest.FunSuite

import CaesarCipher._
class CaesarCipher$Test extends FunSuite {


  test("encode: dupa word") {
    assert(encode("dupa", 1) === "ewrb")
  }

  test("decode: dupa word") {
    assert(decode("ewrb", 1) === "dupa")
  }

  test("encode: dupa word in left encoding") {
    assert(encode("dupa", 1, Left) === "ctoz")
  }

  test("decode: dupa word in left encoding") {
    assert(decode("ctoz", 1, Left) === "dupa")
  }

  // TODO write more tests here for edge cases

}
